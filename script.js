/* ----------------------------------------------------------------------------
Object oriented programming in JavaScript. Create objects containing the same
keys in three different ways with JavaScript. And then add new keys to the
parent (template) object without rewriting the whole code for each newly added
object.

 Exercise: We are organising some event and we are excepting applications,
every new person (object) must have name, age and seat number, and for every
person we must output his seat number (with method).
-------------------------------------------------------------Izabela Ravnikar-*/

//-------------------------------firstWay--------------------------------------

 // CONSTRUCTOR FUNCTION
function Person (name, age, seat) {
    this.name = name;
    this.age = age;
    this.seat = seat;

    // we also want that every person says his seat number
    this.saySeat = function() {
        console.log("Seat number:" + " " + this.seat);
    }
};

// look for instances after all three way are explaind

//-----------------------------secondWay---------------------------------------
/* a slightly different method which differentiates from the first one only in
adding a method */

function Person (name, age, seat) {
    this.name = name;
    this.age = age;
    this.seat = seat;
}
// adding a method in a different way

Person.prototype.saySeat = function () {
    console.log("Seat number:" + " " + this.seat);
}


//---------------------------------thirdWay------------------------------------
/* CLASS SINTAX
The only difference in the syntax of the initialization is using the class
keyword instead of function, and assigning the properties inside a constructor()
method.*/

class Person {
    constructor (name, age, seat) {
        this.name = name;
        this.age = age;
        this.seat = seat;
    }

    // in this case method can be directly added to the class
    saySeat() {
        console.log("seat number:" + " " + this.seat);
    }
};


//---------------------instances and outputs------------------------------------

/* now let's add some participants in the conference

first letter of constructor is capitalized, instances (new people) are written
like a normal variable

const instead of let to prevent reassigment, there cen be two Liams, but are
not the same person. */
const liam = new Person ("Liam", "30", "1");

//let's see if it works
console.log (liam);

//let's see Liam's seat number
liam.saySeat();

// let's add some more...
const luka = new Person ("Luka", "28", "2");
const joanna = new Person ("Joanna", "30", "3");
const julia = new Person ("Julia", "1", "4")


console.log (luka, joanna, julia);

//let's see their seat number
luka.saySeat();
joanna.saySeat();
julia.saySeat();

//==========THERE ARE ALSO ACTIVITIES FOR CHILDREN AT THIS EVENT================

// Creating a new constructor from the parent with call() function

function DayCare (name, age, seat, stage) {
    // Chain constructor with call
    Person.call(this, name, age, seat);

    this.stage = stage;
};


//----------------------------- class syntax------------------------------------

//extends is used to refer to the parent class.
class DayCare extends Person {
    constructor (name, age, seat, stage) {
        //super keyword is used instead of call to access the parent functions
        super (name, age, seat);

        this.stage = stage;
    }

}


//--------------------------instances with added keys---------------------------

const luka = new Person ("Luka", "28", "2", "adult");
const joanna = new Person ("Joanna", "30", "3", "adult");
const julia = new Person ("Julia", "1", "4", "toddler")

console.log (luka, joanna, julia);
